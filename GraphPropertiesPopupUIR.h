/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 1999. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  CURSORS                         1
#define  CURSORS_NUMBERCURSORS           2
#define  CURSORS_CURSORNUMBER            3
#define  CURSORS_POINTSTYLE              4
#define  CURSORS_CROSSHAIRSTYLE          5
#define  CURSORS_CURSORMODE              6
#define  CURSORS_CURSORCOLOR             7

#define  GRAPH                           2
#define  GRAPH_TITLE                     2
#define  GRAPH_LABEL                     3
#define  GRAPH_HEIGHT                    4
#define  GRAPH_WIDTH                     5
#define  GRAPH_GRAPHBACKGROUND           6
#define  GRAPH_PLOTBACKGROUND            7
#define  GRAPH_GRIDCOLOR                 8
#define  GRAPH_EDGESTYLE                 9
#define  GRAPH_SHOWBORDER                10
#define  GRAPH_SHOWINNERLOGMARKERS       11
#define  GRAPH_AXISNAMES                 12
#define  GRAPH_AXISLABELS                13

#define  TABPANEL                        3
#define  TABPANEL_CANVAS                 2
#define  TABPANEL_APPLY                  3
#define  TABPANEL_CANCEL                 4       /* callback function: CancelGraph */
#define  TABPANEL_OK                     5

#define  XAXIS                           4
#define  XAXIS_NAME                      2
#define  XAXIS_OFFSET                    3
#define  XAXIS_GAIN                      4
#define  XAXIS_MINIMUM                   5
#define  XAXIS_MAXIMUM                   6
#define  XAXIS_DIVISIONS                 7
#define  XAXIS_PRECISION                 8
#define  XAXIS_DISPLAYFORMAT             9
#define  XAXIS_ENGUNITS                  10
#define  XAXIS_SHOWGRID                  11
#define  XAXIS_SHOWLABELS                12
#define  XAXIS_REVERSEAXIS               13
#define  XAXIS_AUTOSCALE                 14
#define  XAXIS_AUTODIVISIONS             15
#define  XAXIS_AUTOPRECISION             16
#define  XAXIS_LOGSCALE                  17
#define  XAXIS_MARKORIGIN                18
#define  XAXIS_DECORATION                19
#define  XAXIS_DECORATION_2              20

#define  YAXIS                           5
#define  YAXIS_NAME                      2
#define  YAXIS_OFFSET                    3
#define  YAXIS_GAIN                      4
#define  YAXIS_MINIMUM                   5
#define  YAXIS_MAXIMUM                   6
#define  YAXIS_DIVISIONS                 7
#define  YAXIS_PRECISION                 8
#define  YAXIS_DISPLAYFORMAT             9
#define  YAXIS_ENGUNITS                  10
#define  YAXIS_SHOWGRID                  11
#define  YAXIS_SHOWLABELS                12
#define  YAXIS_REVERSEAXIS               13
#define  YAXIS_AUTOSCALE                 14
#define  YAXIS_AUTODIVISIONS             15
#define  YAXIS_AUTOPRECISION             16
#define  YAXIS_LOGSCALE                  17
#define  YAXIS_MARKORIGIN                18
#define  YAXIS_DECORATION                19
#define  YAXIS_DECORATION_2              20


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK CancelGraph(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
