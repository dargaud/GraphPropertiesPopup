/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/* Copyright (c) National Instruments 1999. All Rights Reserved.          */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PANEL                           1       /* callback function: DemoPanel */
#define  PANEL_GRAPHPROP2                2       /* callback function: GraphProperties2 */
#define  PANEL_GRAPHPROP1                3       /* callback function: GraphProperties1 */
#define  PANEL_GRAPH2                    4
#define  PANEL_GRAPH1                    5


     /* Menu Bars, Menus, and Menu Items: */

          /* (no menu bars in the resource file) */


     /* Callback Prototypes: */ 

int  CVICALLBACK DemoPanel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK GraphProperties1(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK GraphProperties2(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
