#include "easytab.h"
#include <userint.h>
#include "toolbox.h"
#include "GraphPropertiesPopup.h"
#include "GraphPropertiesPopupUIR.h"
typedef struct
{
    int CursorNumber;       //the number of this cursor
	int Color;		//the color of this cursor
	int Mode;				//the mode of this cursor(freeform or snap-to-point)
    int CrosshairStyle;     //the style of the crosshair
    int PointStyle;        	//the point style for the cursor
} CursorType;
typedef struct 
{
    int Bold;
	int Underline;
	int StrikeOut;
	int Italic;
	int TextColor;
	int FontSize;
} FontType;
typedef struct 
{
    double AxisGain;
	double AxisOffset;
	int Divisions;
	int EngineeringUnits;
	int Format;
	int MapMode;
	int MarkOrigin;
	char Name[256];
	int Precision;
	int Reverse;
	double Minimum;
	double Maximum;
	int ScalingMode;
	int ShowGrid;
	int ShowLabel;
} AxisType;
typedef struct
{
	int Panel;					//the panel where the graph is located
	int Graph;					//the control id of the graph
	int Height;					//height of the graph in pixels
	int Width;					//width of the graph in pixels
	int GraphBackgroundColor;	//color of graph background
	int GridColor;				//color of grid
	int PlotBackgroundColor;	//color of plot background
	int ShowInnerLogMarkers;	//whether to show the inner markers if the axes are log style
	int ShowBorder;				//whether to show the border area
    char GraphTitle[256];		//title of the graph
	int EdgeStyle;				//whether edge is raised,flat,or recessed
	FontType GraphLabel;			//all the font attributes of the graph title
	FontType AxisNames;			//all the font attributes of the axis names
	FontType AxisLabels;		//all the font attributes of the axis labels
	AxisType XAxis;				//all the attributes of the X axis
	AxisType YAxis;				//all the attributes of the Y axis  
	ListType Cursors;			//all the cursors and their attributes 
} GraphType;

static int CVICALLBACK ChangeGraphShowBorderArea(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeGraphTitle(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeGraphLabel(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeGraphHeight(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeGraphWidth(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeGraphPlotBackgroundColor(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeGraphBackgroundColor(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeGraphGridColor(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeGraphEdgeStyle(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeGraphShowInnerLogMarkers(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeAxisLabels(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeAxisNames(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK OKGraph(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ApplyGraph(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeXAxis(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeYAxis(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeNumberCursors(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeCursorNumber(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int CVICALLBACK ChangeCursors(int panel,int control,int event,void *callbackData,int eventData1,int eventData2);
static int InitGraphAttributes(int panel,int Control,int GraphPanel,int XAxisPanel,int YAxisPanel,int CursorPanel,GraphType *ThisGraph);

static int GraphPanel,XAxisPanel,YAxisPanel,CursorPanel,TabPanel,EasyTabControl;
static GraphType GlobalGraphProperties;
int GraphPropertiesPopup(int panel,int control,int MinimumCursors)
{
	int NumberCursors;
	TabPanel=LoadPanel(0,"GraphPropertiesPopupUIR.uir",TABPANEL);
    EasyTabControl=EasyTab_ConvertFromCanvas (TabPanel,TABPANEL_CANVAS);
    EasyTab_LoadPanels (TabPanel,EasyTabControl,1,"GraphPropertiesPopupUIR.uir",__CVIUserHInst,
    	GRAPH,&GraphPanel,XAXIS,&XAxisPanel,YAXIS,&YAxisPanel,CURSORS,&CursorPanel,0);
    GetCtrlAttribute (panel, control, ATTR_NUM_CURSORS,&NumberCursors);
    if (NumberCursors>=MinimumCursors) SetCtrlAttribute(CursorPanel,CURSORS_NUMBERCURSORS,ATTR_MIN_VALUE,MinimumCursors);
    InitGraphAttributes(panel,control,GraphPanel,XAxisPanel,YAxisPanel,CursorPanel,&GlobalGraphProperties);
    InstallPopup(TabPanel);
	return 0;
}

//important callbacks
static int InitGraphAttributes(int panel,int graph,int GraphPanel,int XAxisPanel,int YAxisPanel,int CursorPanel,GraphType *ThisGraph)
{
//divided into three sections- get all attributes, set up all chaining, set all popup values
	int i,NumberCursors;
    CursorType ThisCursor;
    ThisGraph->Panel=panel;
    ThisGraph->Graph=graph;
//get the graph attributes    
	GetCtrlAttribute(panel,graph,ATTR_GRAPH_BGCOLOR,&ThisGraph->GraphBackgroundColor);
	GetCtrlAttribute(panel,graph,ATTR_HEIGHT,&ThisGraph->Height);
	GetCtrlAttribute(panel,graph,ATTR_WIDTH,&ThisGraph->Width);
	GetCtrlAttribute(panel,graph,ATTR_GRID_COLOR,&ThisGraph->GridColor);
	GetCtrlAttribute(panel,graph,ATTR_PLOT_BGCOLOR,&ThisGraph->PlotBackgroundColor);
	GetCtrlAttribute(panel,graph,ATTR_INNER_LOG_MARKERS_VISIBLE,&ThisGraph->ShowInnerLogMarkers);
	GetCtrlAttribute(panel,graph,ATTR_BORDER_VISIBLE,&ThisGraph->ShowBorder);
	GetCtrlAttribute(panel,graph,ATTR_LABEL_TEXT,ThisGraph->GraphTitle);
    GetCtrlAttribute(panel,graph,ATTR_EDGE_STYLE,&ThisGraph->EdgeStyle);

	GetCtrlAttribute(panel,graph,ATTR_LABEL_BOLD,&(ThisGraph->GraphLabel.Bold));
	GetCtrlAttribute(panel,graph,ATTR_LABEL_UNDERLINE,&ThisGraph->GraphLabel.Underline);
	GetCtrlAttribute(panel,graph,ATTR_LABEL_STRIKEOUT,&ThisGraph->GraphLabel.StrikeOut);
	GetCtrlAttribute(panel,graph,ATTR_LABEL_ITALIC,&ThisGraph->GraphLabel.Italic);
	GetCtrlAttribute(panel,graph,ATTR_LABEL_COLOR,&ThisGraph->GraphLabel.TextColor);
	GetCtrlAttribute(panel,graph,ATTR_LABEL_POINT_SIZE,&ThisGraph->GraphLabel.FontSize);

	GetCtrlAttribute(panel,graph,ATTR_XYNAME_BOLD,&(ThisGraph->AxisNames.Bold));
	GetCtrlAttribute(panel,graph,ATTR_XYNAME_UNDERLINE,&ThisGraph->AxisNames.Underline);
	GetCtrlAttribute(panel,graph,ATTR_XYNAME_STRIKEOUT,&ThisGraph->AxisNames.StrikeOut);
	GetCtrlAttribute(panel,graph,ATTR_XYNAME_ITALIC,&ThisGraph->AxisNames.Italic);
	GetCtrlAttribute(panel,graph,ATTR_XYNAME_COLOR,&ThisGraph->AxisNames.TextColor);
	GetCtrlAttribute(panel,graph,ATTR_XYNAME_POINT_SIZE,&ThisGraph->AxisNames.FontSize);

	GetCtrlAttribute(panel,graph,ATTR_XYLABEL_BOLD,&ThisGraph->AxisLabels.Bold);
	GetCtrlAttribute(panel,graph,ATTR_XYLABEL_UNDERLINE,&ThisGraph->AxisLabels.Underline);
	GetCtrlAttribute(panel,graph,ATTR_XYLABEL_STRIKEOUT,&ThisGraph->AxisLabels.StrikeOut);
	GetCtrlAttribute(panel,graph,ATTR_XYLABEL_ITALIC,&ThisGraph->AxisLabels.Italic);
	GetCtrlAttribute(panel,graph,ATTR_XYLABEL_COLOR,&ThisGraph->AxisLabels.TextColor);
	GetCtrlAttribute(panel,graph,ATTR_XYLABEL_POINT_SIZE,&ThisGraph->AxisLabels.FontSize);
//get the cursor attributes
	ThisGraph->Cursors=ListCreate(sizeof(CursorType));
	GetCtrlAttribute (panel,graph,ATTR_NUM_CURSORS,&NumberCursors);
	if (NumberCursors>0)
	{
		SetCtrlAttribute(CursorPanel,CURSORS_CURSORNUMBER,ATTR_DIMMED,0);
		SetCtrlAttribute(CursorPanel,CURSORS_POINTSTYLE,ATTR_DIMMED,0);
		SetCtrlAttribute(CursorPanel,CURSORS_CROSSHAIRSTYLE,ATTR_DIMMED,0);
		SetCtrlAttribute(CursorPanel,CURSORS_CURSORMODE,ATTR_DIMMED,0);
		SetCtrlAttribute(CursorPanel,CURSORS_CURSORCOLOR,ATTR_DIMMED,0);
	}
	for (i=1;i<=NumberCursors;i++)
	{
		GetCursorAttribute (panel,graph,i,ATTR_CURSOR_COLOR,&ThisCursor.Color);
		GetCursorAttribute (panel,graph,i,ATTR_CROSS_HAIR_STYLE,&ThisCursor.CrosshairStyle);
		GetCursorAttribute (panel,graph,i,ATTR_CURSOR_MODE,&ThisCursor.Mode);
		GetCursorAttribute (panel,graph,i,ATTR_CURSOR_POINT_STYLE,&ThisCursor.PointStyle);
		ListInsertItem (ThisGraph->Cursors,&ThisCursor,END_OF_LIST);
	}
//get the axis attributes
	GetCtrlAttribute(panel,graph,ATTR_XAXIS_GAIN,&ThisGraph->XAxis.AxisGain);
	GetCtrlAttribute(panel,graph,ATTR_XAXIS_OFFSET,&ThisGraph->XAxis.AxisOffset);
	GetCtrlAttribute(panel,graph,ATTR_XDIVISIONS,&ThisGraph->XAxis.Divisions);
	GetCtrlAttribute(panel,graph,ATTR_XENG_UNITS,&ThisGraph->XAxis.EngineeringUnits);
	GetCtrlAttribute(panel,graph,ATTR_XFORMAT,&ThisGraph->XAxis.Format);
	GetCtrlAttribute(panel,graph,ATTR_XMAP_MODE,&ThisGraph->XAxis.MapMode);
	GetCtrlAttribute(panel,graph,ATTR_XMARK_ORIGIN,&ThisGraph->XAxis.MarkOrigin);
	GetCtrlAttribute(panel,graph,ATTR_XNAME,ThisGraph->XAxis.Name);
	GetCtrlAttribute(panel,graph,ATTR_XPRECISION,&ThisGraph->XAxis.Precision);
	GetCtrlAttribute(panel,graph,ATTR_XREVERSE,&ThisGraph->XAxis.Reverse);
	GetCtrlAttribute(panel,graph,ATTR_XLABEL_VISIBLE,&ThisGraph->XAxis.ShowLabel);
	GetCtrlAttribute(panel,graph,ATTR_XGRID_VISIBLE,&ThisGraph->XAxis.ShowGrid);

	GetCtrlAttribute(panel,graph,ATTR_YAXIS_GAIN,&ThisGraph->YAxis.AxisGain);
	GetCtrlAttribute(panel,graph,ATTR_YAXIS_OFFSET,&ThisGraph->YAxis.AxisOffset);
	GetCtrlAttribute(panel,graph,ATTR_YDIVISIONS,&ThisGraph->YAxis.Divisions);
	GetCtrlAttribute(panel,graph,ATTR_YENG_UNITS,&ThisGraph->YAxis.EngineeringUnits);
	GetCtrlAttribute(panel,graph,ATTR_YFORMAT,&ThisGraph->YAxis.Format);
	GetCtrlAttribute(panel,graph,ATTR_YMAP_MODE,&ThisGraph->YAxis.MapMode);
	GetCtrlAttribute(panel,graph,ATTR_YMARK_ORIGIN,&ThisGraph->YAxis.MarkOrigin);
	GetCtrlAttribute(panel,graph,ATTR_YNAME,ThisGraph->YAxis.Name);
	GetCtrlAttribute(panel,graph,ATTR_YPRECISION,&ThisGraph->YAxis.Precision);
	GetCtrlAttribute(panel,graph,ATTR_YREVERSE,&ThisGraph->YAxis.Reverse);
	GetCtrlAttribute(panel,graph,ATTR_YLABEL_VISIBLE,&ThisGraph->YAxis.ShowLabel);
	GetCtrlAttribute(panel,graph,ATTR_YGRID_VISIBLE,&ThisGraph->YAxis.ShowGrid);

	GetAxisScalingMode(panel,graph,VAL_XAXIS,&ThisGraph->XAxis.ScalingMode,
		&ThisGraph->XAxis.Minimum,&ThisGraph->XAxis.Maximum);
	GetAxisScalingMode(panel,graph,VAL_LEFT_YAXIS,&ThisGraph->YAxis.ScalingMode,
		&ThisGraph->YAxis.Minimum,&ThisGraph->YAxis.Maximum);
//set up chaining
    ChainCtrlCallback(GraphPanel,GRAPH_GRAPHBACKGROUND,ChangeGraphBackgroundColor,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(GraphPanel,GRAPH_GRIDCOLOR,ChangeGraphGridColor,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(GraphPanel,GRAPH_HEIGHT,ChangeGraphHeight,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(GraphPanel,GRAPH_WIDTH,ChangeGraphWidth,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(GraphPanel,GRAPH_PLOTBACKGROUND,ChangeGraphPlotBackgroundColor,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(GraphPanel,GRAPH_SHOWINNERLOGMARKERS,ChangeGraphShowInnerLogMarkers,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(GraphPanel,GRAPH_SHOWBORDER,ChangeGraphShowBorderArea,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(GraphPanel,GRAPH_EDGESTYLE,ChangeGraphEdgeStyle,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(GraphPanel,GRAPH_LABEL,ChangeGraphLabel,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(GraphPanel,GRAPH_AXISNAMES,ChangeAxisNames,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(GraphPanel,GRAPH_AXISLABELS,ChangeAxisLabels,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(GraphPanel,GRAPH_TITLE,ChangeGraphTitle,ThisGraph,"Modifying the Graph");

    ChainCtrlCallback(CursorPanel,CURSORS_NUMBERCURSORS,ChangeNumberCursors,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(CursorPanel,CURSORS_CURSORNUMBER,ChangeCursorNumber,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(CursorPanel,CURSORS_POINTSTYLE,ChangeCursors,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(CursorPanel,CURSORS_CROSSHAIRSTYLE,ChangeCursors,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(CursorPanel,CURSORS_CURSORCOLOR,ChangeCursors,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(CursorPanel,CURSORS_CURSORMODE,ChangeCursors,ThisGraph,"Modifying the Graph");

    ChainCtrlCallback(XAxisPanel,XAXIS_REVERSEAXIS,ChangeXAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(XAxisPanel,XAXIS_SHOWLABELS,ChangeXAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(XAxisPanel,XAXIS_SHOWGRID,ChangeXAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(XAxisPanel,XAXIS_AUTOPRECISION,ChangeXAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(XAxisPanel,XAXIS_AUTOSCALE,ChangeXAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(XAxisPanel,XAXIS_MARKORIGIN,ChangeXAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(XAxisPanel,XAXIS_LOGSCALE,ChangeXAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(XAxisPanel,XAXIS_AUTODIVISIONS,ChangeXAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(XAxisPanel,XAXIS_NAME,ChangeXAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(XAxisPanel,XAXIS_PRECISION,ChangeXAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(XAxisPanel,XAXIS_ENGUNITS,ChangeXAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(XAxisPanel,XAXIS_DIVISIONS,ChangeXAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(XAxisPanel,XAXIS_GAIN,ChangeXAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(XAxisPanel,XAXIS_OFFSET,ChangeXAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(XAxisPanel,XAXIS_MAXIMUM,ChangeXAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(XAxisPanel,XAXIS_MINIMUM,ChangeXAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(XAxisPanel,XAXIS_DISPLAYFORMAT,ChangeXAxis,ThisGraph,"Modifying the Graph");

    ChainCtrlCallback(YAxisPanel,YAXIS_REVERSEAXIS,ChangeYAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(YAxisPanel,YAXIS_SHOWLABELS,ChangeYAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(YAxisPanel,YAXIS_SHOWGRID,ChangeYAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(YAxisPanel,YAXIS_AUTOPRECISION,ChangeYAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(YAxisPanel,YAXIS_AUTOSCALE,ChangeYAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(YAxisPanel,YAXIS_MARKORIGIN,ChangeYAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(YAxisPanel,YAXIS_LOGSCALE,ChangeYAxis,ThisGraph,"Modifying the Graph");
    ChainCtrlCallback(YAxisPanel,YAXIS_AUTODIVISIONS,ChangeYAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(YAxisPanel,YAXIS_NAME,ChangeYAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(YAxisPanel,YAXIS_PRECISION,ChangeYAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(YAxisPanel,YAXIS_ENGUNITS,ChangeYAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(YAxisPanel,YAXIS_DIVISIONS,ChangeYAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(YAxisPanel,YAXIS_GAIN,ChangeYAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(YAxisPanel,YAXIS_OFFSET,ChangeYAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(YAxisPanel,YAXIS_MAXIMUM,ChangeYAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(YAxisPanel,YAXIS_MINIMUM,ChangeYAxis,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(YAxisPanel,YAXIS_DISPLAYFORMAT,ChangeYAxis,ThisGraph,"Modifying the Graph");

	ChainCtrlCallback(TabPanel,TABPANEL_OK,OKGraph,ThisGraph,"Modifying the Graph");
	ChainCtrlCallback(TabPanel,TABPANEL_APPLY,ApplyGraph,ThisGraph,"Modifying the Graph");
//set up graph properties values
    SetCtrlVal(GraphPanel,GRAPH_GRAPHBACKGROUND,ThisGraph->GraphBackgroundColor);
    SetCtrlVal(GraphPanel,GRAPH_GRIDCOLOR,ThisGraph->GridColor);
    SetCtrlVal(GraphPanel,GRAPH_PLOTBACKGROUND,ThisGraph->PlotBackgroundColor);
    SetCtrlVal(GraphPanel,GRAPH_SHOWINNERLOGMARKERS,ThisGraph->ShowInnerLogMarkers);
    SetCtrlVal(GraphPanel,GRAPH_SHOWBORDER,ThisGraph->ShowBorder);
    SetCtrlVal(GraphPanel,GRAPH_EDGESTYLE,ThisGraph->EdgeStyle);
	SetCtrlVal(GraphPanel,GRAPH_HEIGHT,ThisGraph->Height);
	SetCtrlVal(GraphPanel,GRAPH_WIDTH,ThisGraph->Width);
	SetCtrlVal(GraphPanel,GRAPH_TITLE,ThisGraph->GraphTitle);
//set up cursor properties values
	if(NumberCursors==0)
	{
		SetCtrlAttribute(CursorPanel,CURSORS_CURSORNUMBER,ATTR_MIN_VALUE,0);
		SetCtrlAttribute(CursorPanel,CURSORS_CURSORNUMBER,ATTR_MAX_VALUE,0);
		SetCtrlVal(CursorPanel,CURSORS_CURSORNUMBER,0);
	}
	else
	{
		SetCtrlAttribute(CursorPanel,CURSORS_CURSORNUMBER,ATTR_DIMMED,0);
		SetCtrlAttribute(CursorPanel,CURSORS_POINTSTYLE,ATTR_DIMMED,0);
		SetCtrlAttribute(CursorPanel,CURSORS_CROSSHAIRSTYLE,ATTR_DIMMED,0);
		SetCtrlAttribute(CursorPanel,CURSORS_CURSORMODE,ATTR_DIMMED,0);
		SetCtrlAttribute(CursorPanel,CURSORS_CURSORCOLOR,ATTR_DIMMED,0);
		ListGetItem(ThisGraph->Cursors, &ThisCursor, FRONT_OF_LIST);
		SetCtrlVal(CursorPanel,CURSORS_NUMBERCURSORS,NumberCursors);
		SetCtrlVal(CursorPanel,CURSORS_CURSORNUMBER,1);
		SetCtrlVal(CursorPanel,CURSORS_POINTSTYLE,ThisCursor.PointStyle);
		SetCtrlVal(CursorPanel,CURSORS_CROSSHAIRSTYLE,ThisCursor.CrosshairStyle);
		SetCtrlVal(CursorPanel,CURSORS_CURSORMODE,ThisCursor.Mode);
		SetCtrlVal(CursorPanel,CURSORS_CURSORCOLOR,ThisCursor.Color);
		SetCtrlAttribute(CursorPanel,CURSORS_CURSORNUMBER,ATTR_MAX_VALUE,NumberCursors);
		SetCtrlAttribute(CursorPanel,CURSORS_CURSORNUMBER,ATTR_MIN_VALUE,1);
	}
//set up X axis properties
	SetCtrlVal(XAxisPanel,XAXIS_OFFSET,ThisGraph->XAxis.AxisOffset);
	SetCtrlVal(XAxisPanel,XAXIS_GAIN,ThisGraph->XAxis.AxisGain);
	SetCtrlVal(XAxisPanel,XAXIS_DISPLAYFORMAT,ThisGraph->XAxis.Format);
	SetCtrlVal(XAxisPanel,XAXIS_NAME,ThisGraph->XAxis.Name);
	SetCtrlVal(XAxisPanel,XAXIS_ENGUNITS,ThisGraph->XAxis.EngineeringUnits);
	SetCtrlVal(XAxisPanel,XAXIS_LOGSCALE,ThisGraph->XAxis.MapMode);
	SetCtrlVal(XAxisPanel,XAXIS_MARKORIGIN,ThisGraph->XAxis.MarkOrigin);
	SetCtrlVal(XAxisPanel,XAXIS_REVERSEAXIS,ThisGraph->XAxis.Reverse);
	SetCtrlVal(XAxisPanel,XAXIS_SHOWLABELS,ThisGraph->XAxis.ShowLabel);
	SetCtrlVal(XAxisPanel,XAXIS_SHOWGRID,ThisGraph->XAxis.ShowGrid);
	if (ThisGraph->XAxis.MapMode==1)
	{
		SetCtrlVal(XAxisPanel,XAXIS_MARKORIGIN,0);
		SetCtrlAttribute(XAxisPanel,XAXIS_MARKORIGIN,ATTR_DIMMED,1);
	}
	else
	{
		SetCtrlAttribute(XAxisPanel,XAXIS_MARKORIGIN,ATTR_DIMMED,0);
	}
	if (ThisGraph->XAxis.Divisions==VAL_AUTO)
	{
		SetCtrlVal(XAxisPanel,XAXIS_AUTODIVISIONS,1);
		SetCtrlAttribute(XAxisPanel,XAXIS_DIVISIONS,ATTR_DIMMED,1);
	}
	else 
	{
		SetCtrlVal(XAxisPanel,XAXIS_AUTODIVISIONS,0);
		SetCtrlAttribute(XAxisPanel,XAXIS_DIVISIONS,ATTR_DIMMED,0);
		SetCtrlVal(XAxisPanel,XAXIS_DIVISIONS,ThisGraph->XAxis.Divisions);
	}
	if (ThisGraph->XAxis.Precision==VAL_AUTO)
	{
		SetCtrlVal(XAxisPanel,XAXIS_AUTOPRECISION,1);
		SetCtrlAttribute(XAxisPanel,XAXIS_PRECISION,ATTR_DIMMED,1);
	}
	else 
	{
		SetCtrlVal(XAxisPanel,XAXIS_AUTOPRECISION,0);
		SetCtrlAttribute(XAxisPanel,XAXIS_PRECISION,ATTR_DIMMED,0);
		SetCtrlVal(XAxisPanel,XAXIS_PRECISION,ThisGraph->XAxis.Precision);
	}
	if (ThisGraph->XAxis.ScalingMode==VAL_AUTOSCALE)
	{
		SetCtrlVal(XAxisPanel,XAXIS_AUTOSCALE,1);
		SetCtrlAttribute(XAxisPanel,XAXIS_MINIMUM,ATTR_DIMMED,1);
		SetCtrlAttribute(XAxisPanel,XAXIS_MAXIMUM,ATTR_DIMMED,1);
	}
	else 
	{
		SetCtrlVal(XAxisPanel,XAXIS_AUTOSCALE,0);
		SetCtrlAttribute(XAxisPanel,XAXIS_MINIMUM,ATTR_DIMMED,0);
		SetCtrlAttribute(XAxisPanel,XAXIS_MAXIMUM,ATTR_DIMMED,0);
		SetCtrlVal(XAxisPanel,XAXIS_MINIMUM,ThisGraph->XAxis.Minimum);
		SetCtrlVal(XAxisPanel,XAXIS_MAXIMUM,ThisGraph->XAxis.Maximum);
	}
	if ((ThisGraph->XAxis.ScalingMode!=VAL_AUTOSCALE)&&(ThisGraph->XAxis.Minimum<=0))
	{
		SetCtrlVal(XAxisPanel,XAXIS_LOGSCALE,0);
		SetCtrlAttribute(XAxisPanel,XAXIS_LOGSCALE,ATTR_DIMMED,1);
	}
	else 
	{
		SetCtrlAttribute(XAxisPanel,XAXIS_LOGSCALE,ATTR_DIMMED,0);
	}
//set up Y axis properties
	SetCtrlVal(YAxisPanel,YAXIS_OFFSET,ThisGraph->YAxis.AxisOffset);
	SetCtrlVal(YAxisPanel,YAXIS_GAIN,ThisGraph->YAxis.AxisGain);
	SetCtrlVal(YAxisPanel,YAXIS_DISPLAYFORMAT,ThisGraph->YAxis.Format);
	SetCtrlVal(YAxisPanel,YAXIS_NAME,ThisGraph->YAxis.Name);
	SetCtrlVal(YAxisPanel,YAXIS_ENGUNITS,ThisGraph->YAxis.EngineeringUnits);
	SetCtrlVal(YAxisPanel,YAXIS_LOGSCALE,ThisGraph->YAxis.MapMode);
	SetCtrlVal(YAxisPanel,YAXIS_MARKORIGIN,ThisGraph->YAxis.MarkOrigin);
	SetCtrlVal(YAxisPanel,YAXIS_REVERSEAXIS,ThisGraph->YAxis.Reverse);
	SetCtrlVal(YAxisPanel,YAXIS_SHOWLABELS,ThisGraph->YAxis.ShowLabel);
	SetCtrlVal(YAxisPanel,YAXIS_SHOWGRID,ThisGraph->YAxis.ShowGrid);
	if (ThisGraph->YAxis.MapMode==1)
	{
		SetCtrlVal(YAxisPanel,YAXIS_MARKORIGIN,0);
		SetCtrlAttribute(YAxisPanel,YAXIS_MARKORIGIN,ATTR_DIMMED,1);
	}
	else
	{
		SetCtrlAttribute(YAxisPanel,YAXIS_MARKORIGIN,ATTR_DIMMED,0);
	}
	if (ThisGraph->YAxis.Divisions==VAL_AUTO)
	{
		SetCtrlVal(YAxisPanel,YAXIS_AUTODIVISIONS,1);
		SetCtrlAttribute(YAxisPanel,YAXIS_DIVISIONS,ATTR_DIMMED,1);
	}
	else 
	{
		SetCtrlVal(YAxisPanel,YAXIS_AUTODIVISIONS,0);
		SetCtrlAttribute(YAxisPanel,YAXIS_DIVISIONS,ATTR_DIMMED,0);
		SetCtrlVal(YAxisPanel,YAXIS_DIVISIONS,ThisGraph->YAxis.Divisions);
	}
	if (ThisGraph->YAxis.Precision==VAL_AUTO)
	{
		SetCtrlVal(YAxisPanel,YAXIS_AUTOPRECISION,1);
		SetCtrlAttribute(YAxisPanel,YAXIS_PRECISION,ATTR_DIMMED,1);
	}
	else 
	{
		SetCtrlVal(YAxisPanel,YAXIS_AUTOPRECISION,0);
		SetCtrlAttribute(YAxisPanel,YAXIS_PRECISION,ATTR_DIMMED,0);
		SetCtrlVal(YAxisPanel,YAXIS_PRECISION,ThisGraph->YAxis.Precision);
	}
	if (ThisGraph->YAxis.ScalingMode==VAL_AUTOSCALE)
	{
		SetCtrlVal(YAxisPanel,YAXIS_AUTOSCALE,1);
		SetCtrlAttribute(YAxisPanel,YAXIS_MINIMUM,ATTR_DIMMED,1);
		SetCtrlAttribute(YAxisPanel,YAXIS_MAXIMUM,ATTR_DIMMED,1);
	}
	else 
	{
		SetCtrlVal(YAxisPanel,YAXIS_AUTOSCALE,0);
		SetCtrlAttribute(YAxisPanel,YAXIS_MINIMUM,ATTR_DIMMED,0);
		SetCtrlAttribute(YAxisPanel,YAXIS_MAXIMUM,ATTR_DIMMED,0);
		SetCtrlVal(YAxisPanel,YAXIS_MINIMUM,ThisGraph->YAxis.Minimum);
		SetCtrlVal(YAxisPanel,YAXIS_MAXIMUM,ThisGraph->YAxis.Maximum);
	}
	if ((ThisGraph->YAxis.ScalingMode!=VAL_AUTOSCALE)&&(ThisGraph->YAxis.Minimum<=0))
	{
		SetCtrlVal(YAxisPanel,YAXIS_LOGSCALE,0);
		SetCtrlAttribute(YAxisPanel,YAXIS_LOGSCALE,ATTR_DIMMED,1);
	}
	else 
	{
		SetCtrlAttribute(YAxisPanel,YAXIS_LOGSCALE,ATTR_DIMMED,0);
	}

return 0;
}
static int CVICALLBACK ChangeGraphShowBorderArea(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,control,&ThisGraph->ShowBorder);
	}
	return 0;
}
static int CVICALLBACK ChangeGraphTitle(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,control,ThisGraph->GraphTitle);
	}
	return 0;
}
static int CVICALLBACK ChangeGraphLabel(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		FontSelectPopup("Select Tab Font","Sample Text",0,0,&(ThisGraph->GraphLabel.Bold),&ThisGraph->GraphLabel.Underline,
			&ThisGraph->GraphLabel.StrikeOut,&ThisGraph->GraphLabel.Italic,0,&ThisGraph->GraphLabel.TextColor,
			&ThisGraph->GraphLabel.FontSize,2,48,TRUE,TRUE);
	}
	return 0;
}
static int CVICALLBACK ChangeGraphHeight(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,control,&ThisGraph->Height);
	}
	return 0;
}
static int CVICALLBACK ChangeGraphWidth(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,control,&ThisGraph->Width);
	}
	return 0;
}
static int CVICALLBACK ChangeGraphPlotBackgroundColor(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,control,&ThisGraph->PlotBackgroundColor);
	}
	return 0;
}
static int CVICALLBACK ChangeGraphBackgroundColor(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,control,&ThisGraph->GraphBackgroundColor);
	}
	return 0;
}
static int CVICALLBACK ChangeGraphGridColor(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,control,&ThisGraph->GridColor);
	}
	return 0;
}
static int CVICALLBACK ChangeGraphEdgeStyle(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
	if(event==EVENT_COMMIT)
	{
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,control,&ThisGraph->EdgeStyle);
	}
	return 0;
}
static int CVICALLBACK ChangeGraphShowInnerLogMarkers(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,control,&ThisGraph->ShowInnerLogMarkers);
	}
	return 0;
}
static int CVICALLBACK ChangeAxisLabels(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		FontSelectPopup("Select Tab Font","Sample Text",0,0,&(ThisGraph->AxisLabels.Bold),&ThisGraph->AxisLabels.Underline,
			&ThisGraph->AxisLabels.StrikeOut,&ThisGraph->AxisLabels.Italic,0,&ThisGraph->AxisLabels.TextColor,
			&ThisGraph->AxisLabels.FontSize,2,48,TRUE,TRUE);
	}
	return 0;
}
static int CVICALLBACK ChangeAxisNames(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		FontSelectPopup("Select Tab Font","Sample Text",0,0,&(ThisGraph->AxisNames.Bold),&ThisGraph->AxisNames.Underline,
			&ThisGraph->AxisNames.StrikeOut,&ThisGraph->AxisNames.Italic,0,&ThisGraph->AxisNames.TextColor,
			&ThisGraph->AxisNames.FontSize,2,48,TRUE,TRUE);
	}
	return 0;
}
//changing axes
static int CVICALLBACK ChangeXAxis (int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    int autodivisions,autoprecision;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(XAxisPanel,XAXIS_OFFSET,&ThisGraph->XAxis.AxisOffset);
		GetCtrlVal(XAxisPanel,XAXIS_GAIN,&ThisGraph->XAxis.AxisGain);
		GetCtrlVal(XAxisPanel,XAXIS_DISPLAYFORMAT,&ThisGraph->XAxis.Format);
		GetCtrlVal(XAxisPanel,XAXIS_NAME,ThisGraph->XAxis.Name);
		GetCtrlVal(XAxisPanel,XAXIS_ENGUNITS,&ThisGraph->XAxis.EngineeringUnits);
		GetCtrlVal(XAxisPanel,XAXIS_LOGSCALE,&ThisGraph->XAxis.MapMode);
		GetCtrlVal(XAxisPanel,XAXIS_MARKORIGIN,&ThisGraph->XAxis.MarkOrigin);
		GetCtrlVal(XAxisPanel,XAXIS_REVERSEAXIS,&ThisGraph->XAxis.Reverse);
		GetCtrlVal(XAxisPanel,XAXIS_SHOWLABELS,&ThisGraph->XAxis.ShowLabel);
		GetCtrlVal(XAxisPanel,XAXIS_SHOWGRID,&ThisGraph->XAxis.ShowGrid);
		GetCtrlVal(XAxisPanel,XAXIS_DIVISIONS,&ThisGraph->XAxis.Divisions);
		GetCtrlVal(XAxisPanel,XAXIS_PRECISION,&ThisGraph->XAxis.Precision);
		GetCtrlVal(XAxisPanel,XAXIS_MINIMUM,&ThisGraph->XAxis.Minimum);
		GetCtrlVal(XAxisPanel,XAXIS_MAXIMUM,&ThisGraph->XAxis.Maximum);
		GetCtrlVal(XAxisPanel,XAXIS_AUTOSCALE,&ThisGraph->XAxis.ScalingMode);
		GetCtrlVal(XAxisPanel,XAXIS_AUTODIVISIONS,&autodivisions);
		GetCtrlVal(XAxisPanel,XAXIS_AUTOPRECISION,&autoprecision);
		if (autodivisions==1) ThisGraph->XAxis.Divisions=VAL_AUTO;
		if (autoprecision==1) ThisGraph->XAxis.Precision=VAL_AUTO;
		if (ThisGraph->XAxis.MapMode==1) SetCtrlAttribute(XAxisPanel,XAXIS_MARKORIGIN,ATTR_DIMMED,1);
		else SetCtrlAttribute(XAxisPanel,XAXIS_MARKORIGIN,ATTR_DIMMED,0);
		if (ThisGraph->XAxis.Divisions==VAL_AUTO) SetCtrlAttribute(XAxisPanel,XAXIS_DIVISIONS,ATTR_DIMMED,1);
		else SetCtrlAttribute(XAxisPanel,XAXIS_DIVISIONS,ATTR_DIMMED,0);
		if (ThisGraph->XAxis.Precision==VAL_AUTO) SetCtrlAttribute(XAxisPanel,XAXIS_PRECISION,ATTR_DIMMED,1);
		else SetCtrlAttribute(XAxisPanel,XAXIS_PRECISION,ATTR_DIMMED,0);
		if (ThisGraph->XAxis.ScalingMode==VAL_AUTOSCALE)
		{
			SetCtrlAttribute(XAxisPanel,XAXIS_MINIMUM,ATTR_DIMMED,1);
			SetCtrlAttribute(XAxisPanel,XAXIS_MAXIMUM,ATTR_DIMMED,1);
		}
		else 
		{
			SetCtrlAttribute(XAxisPanel,XAXIS_MINIMUM,ATTR_DIMMED,0);
			SetCtrlAttribute(XAxisPanel,XAXIS_MAXIMUM,ATTR_DIMMED,0);
		}
		if ((ThisGraph->XAxis.ScalingMode!=VAL_AUTOSCALE)&&(ThisGraph->XAxis.Minimum<=0))
		{
			SetCtrlAttribute(XAxisPanel,XAXIS_LOGSCALE,ATTR_DIMMED,1);
		}
		else SetCtrlAttribute(XAxisPanel,XAXIS_LOGSCALE,ATTR_DIMMED,0);
		if (ThisGraph->XAxis.Maximum<=ThisGraph->XAxis.Minimum)
		{
			ThisGraph->XAxis.Maximum=ThisGraph->XAxis.Minimum+1;
			SetCtrlVal(XAxisPanel,XAXIS_MAXIMUM,ThisGraph->XAxis.Maximum);
		}
	}
	return 0;
}
static int CVICALLBACK ChangeYAxis (int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    int autodivisions,autoprecision;
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(YAxisPanel,YAXIS_OFFSET,&ThisGraph->YAxis.AxisOffset);
		GetCtrlVal(YAxisPanel,YAXIS_GAIN,&ThisGraph->YAxis.AxisGain);
		GetCtrlVal(YAxisPanel,YAXIS_DISPLAYFORMAT,&ThisGraph->YAxis.Format);
		GetCtrlVal(YAxisPanel,YAXIS_NAME,ThisGraph->YAxis.Name);
		GetCtrlVal(YAxisPanel,YAXIS_ENGUNITS,&ThisGraph->YAxis.EngineeringUnits);
		GetCtrlVal(YAxisPanel,YAXIS_LOGSCALE,&ThisGraph->YAxis.MapMode);
		GetCtrlVal(YAxisPanel,YAXIS_MARKORIGIN,&ThisGraph->YAxis.MarkOrigin);
		GetCtrlVal(YAxisPanel,YAXIS_REVERSEAXIS,&ThisGraph->YAxis.Reverse);
		GetCtrlVal(YAxisPanel,YAXIS_SHOWLABELS,&ThisGraph->YAxis.ShowLabel);
		GetCtrlVal(YAxisPanel,YAXIS_SHOWGRID,&ThisGraph->YAxis.ShowGrid);
		GetCtrlVal(YAxisPanel,YAXIS_DIVISIONS,&ThisGraph->YAxis.Divisions);
		GetCtrlVal(YAxisPanel,YAXIS_PRECISION,&ThisGraph->YAxis.Precision);
		GetCtrlVal(YAxisPanel,YAXIS_MINIMUM,&ThisGraph->YAxis.Minimum);
		GetCtrlVal(YAxisPanel,YAXIS_MAXIMUM,&ThisGraph->YAxis.Maximum);
		GetCtrlVal(YAxisPanel,YAXIS_AUTOSCALE,&ThisGraph->YAxis.ScalingMode);
		GetCtrlVal(YAxisPanel,YAXIS_AUTODIVISIONS,&autodivisions);
		GetCtrlVal(YAxisPanel,YAXIS_AUTOPRECISION,&autoprecision);
		if (autodivisions==1) ThisGraph->YAxis.Divisions=VAL_AUTO;
		if (autoprecision==1) ThisGraph->YAxis.Precision=VAL_AUTO;
		if (ThisGraph->YAxis.MapMode==1) SetCtrlAttribute(YAxisPanel,YAXIS_MARKORIGIN,ATTR_DIMMED,1);
		else SetCtrlAttribute(YAxisPanel,YAXIS_MARKORIGIN,ATTR_DIMMED,0);
		if (ThisGraph->YAxis.Divisions==VAL_AUTO) SetCtrlAttribute(YAxisPanel,YAXIS_DIVISIONS,ATTR_DIMMED,1);
		else SetCtrlAttribute(YAxisPanel,YAXIS_DIVISIONS,ATTR_DIMMED,0);
		if (ThisGraph->YAxis.Precision==VAL_AUTO) SetCtrlAttribute(YAxisPanel,YAXIS_PRECISION,ATTR_DIMMED,1);
		else SetCtrlAttribute(YAxisPanel,YAXIS_PRECISION,ATTR_DIMMED,0);
		if (ThisGraph->YAxis.ScalingMode==VAL_AUTOSCALE)
		{
			SetCtrlAttribute(YAxisPanel,YAXIS_MINIMUM,ATTR_DIMMED,1);
			SetCtrlAttribute(YAxisPanel,YAXIS_MAXIMUM,ATTR_DIMMED,1);
		}
		else 
		{
			SetCtrlAttribute(YAxisPanel,YAXIS_MINIMUM,ATTR_DIMMED,0);
			SetCtrlAttribute(YAxisPanel,YAXIS_MAXIMUM,ATTR_DIMMED,0);
		}
		if ((ThisGraph->YAxis.ScalingMode!=VAL_AUTOSCALE)&&(ThisGraph->YAxis.Minimum<=0))
		{
			SetCtrlAttribute(YAxisPanel,YAXIS_LOGSCALE,ATTR_DIMMED,1);
		}
		else SetCtrlAttribute(YAxisPanel,YAXIS_LOGSCALE,ATTR_DIMMED,0);
		if (ThisGraph->YAxis.Maximum<=ThisGraph->YAxis.Minimum)
		{
			ThisGraph->YAxis.Maximum=ThisGraph->YAxis.Minimum+1;
			SetCtrlVal(YAxisPanel,YAXIS_MAXIMUM,ThisGraph->YAxis.Maximum);
		}
	}
	return 0;
}
//ok, apply and cancel
static int CVICALLBACK OKGraph(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    CursorType ThisCursor;
    int i;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_HEIGHT,ThisGraph->Height);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_WIDTH,ThisGraph->Width);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_EDGE_STYLE,ThisGraph->EdgeStyle);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_GRAPH_BGCOLOR,ThisGraph->GraphBackgroundColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_GRID_COLOR,ThisGraph->GridColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_INNER_LOG_MARKERS_VISIBLE,ThisGraph->ShowInnerLogMarkers);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_PLOT_BGCOLOR,ThisGraph->PlotBackgroundColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_BORDER_VISIBLE,ThisGraph->ShowBorder);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_TEXT,ThisGraph->GraphTitle);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_BOLD,ThisGraph->GraphLabel.Bold);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_COLOR,ThisGraph->GraphLabel.TextColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_ITALIC,ThisGraph->GraphLabel.Italic);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_POINT_SIZE,ThisGraph->GraphLabel.FontSize);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_STRIKEOUT,ThisGraph->GraphLabel.StrikeOut);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_UNDERLINE,ThisGraph->GraphLabel.Underline);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_BOLD,ThisGraph->AxisNames.Bold);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_COLOR,ThisGraph->AxisNames.TextColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_ITALIC,ThisGraph->AxisNames.Italic);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_POINT_SIZE,ThisGraph->AxisNames.FontSize);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_STRIKEOUT,ThisGraph->AxisNames.StrikeOut);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_UNDERLINE,ThisGraph->AxisNames.Underline);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_BOLD,ThisGraph->AxisLabels.Bold);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_COLOR,ThisGraph->AxisLabels.TextColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_ITALIC,ThisGraph->AxisLabels.Italic);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_POINT_SIZE,ThisGraph->AxisLabels.FontSize);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_STRIKEOUT,ThisGraph->AxisLabels.StrikeOut);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_UNDERLINE,ThisGraph->AxisLabels.Underline);

		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XAXIS_GAIN,ThisGraph->XAxis.AxisGain);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XAXIS_OFFSET,ThisGraph->XAxis.AxisOffset);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XDIVISIONS,ThisGraph->XAxis.Divisions);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XENG_UNITS,ThisGraph->XAxis.EngineeringUnits);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XFORMAT,ThisGraph->XAxis.Format);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XMAP_MODE,ThisGraph->XAxis.MapMode);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XMARK_ORIGIN,ThisGraph->XAxis.MarkOrigin);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XNAME,ThisGraph->XAxis.Name);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XPRECISION,ThisGraph->XAxis.Precision);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XREVERSE,ThisGraph->XAxis.Reverse);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XLABEL_VISIBLE,ThisGraph->XAxis.ShowLabel);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XGRID_VISIBLE,ThisGraph->XAxis.ShowGrid);

		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YAXIS_GAIN,ThisGraph->YAxis.AxisGain);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YAXIS_OFFSET,ThisGraph->YAxis.AxisOffset);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YDIVISIONS,ThisGraph->YAxis.Divisions);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YENG_UNITS,ThisGraph->YAxis.EngineeringUnits);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YFORMAT,ThisGraph->YAxis.Format);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YMAP_MODE,ThisGraph->YAxis.MapMode);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YMARK_ORIGIN,ThisGraph->YAxis.MarkOrigin);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YNAME,ThisGraph->YAxis.Name);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YPRECISION,ThisGraph->YAxis.Precision);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YREVERSE,ThisGraph->YAxis.Reverse);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YLABEL_VISIBLE,ThisGraph->YAxis.ShowLabel);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YGRID_VISIBLE,ThisGraph->YAxis.ShowGrid);

		SetAxisScalingMode(ThisGraph->Panel,ThisGraph->Graph,VAL_XAXIS,ThisGraph->XAxis.ScalingMode,
			ThisGraph->XAxis.Minimum,ThisGraph->XAxis.Maximum);
		SetAxisScalingMode(ThisGraph->Panel,ThisGraph->Graph,VAL_LEFT_YAXIS,ThisGraph->YAxis.ScalingMode,
			ThisGraph->YAxis.Minimum,ThisGraph->YAxis.Maximum);

		SetCtrlAttribute (ThisGraph->Panel,ThisGraph->Graph,ATTR_NUM_CURSORS,ListNumItems (ThisGraph->Cursors));
		for (i=1;i<=ListNumItems (ThisGraph->Cursors);i++)
		{
			ListGetItem(ThisGraph->Cursors,&ThisCursor,i);
			SetCursorAttribute (ThisGraph->Panel,ThisGraph->Graph,i,ATTR_CURSOR_COLOR,ThisCursor.Color);
			SetCursorAttribute (ThisGraph->Panel,ThisGraph->Graph,i,ATTR_CROSS_HAIR_STYLE,ThisCursor.CrosshairStyle);
			SetCursorAttribute (ThisGraph->Panel,ThisGraph->Graph,i,ATTR_CURSOR_MODE,ThisCursor.Mode);
			SetCursorAttribute (ThisGraph->Panel,ThisGraph->Graph,i,ATTR_CURSOR_POINT_STYLE,ThisCursor.PointStyle);
		}

		RemovePopup (0);
    }
	return 0;
}
static int CVICALLBACK ApplyGraph(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
    CursorType ThisCursor;
    int i,NumberCursors;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_HEIGHT,ThisGraph->Height);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_WIDTH,ThisGraph->Width);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_EDGE_STYLE,ThisGraph->EdgeStyle);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_GRAPH_BGCOLOR,ThisGraph->GraphBackgroundColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_GRID_COLOR,ThisGraph->GridColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_INNER_LOG_MARKERS_VISIBLE,ThisGraph->ShowInnerLogMarkers);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_PLOT_BGCOLOR,ThisGraph->PlotBackgroundColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_BORDER_VISIBLE,ThisGraph->ShowBorder);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_TEXT,ThisGraph->GraphTitle);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_BOLD,ThisGraph->GraphLabel.Bold);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_COLOR,ThisGraph->GraphLabel.TextColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_ITALIC,ThisGraph->GraphLabel.Italic);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_POINT_SIZE,ThisGraph->GraphLabel.FontSize);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_STRIKEOUT,ThisGraph->GraphLabel.StrikeOut);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_LABEL_UNDERLINE,ThisGraph->GraphLabel.Underline);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_BOLD,ThisGraph->AxisNames.Bold);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_COLOR,ThisGraph->AxisNames.TextColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_ITALIC,ThisGraph->AxisNames.Italic);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_POINT_SIZE,ThisGraph->AxisNames.FontSize);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_STRIKEOUT,ThisGraph->AxisNames.StrikeOut);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYNAME_UNDERLINE,ThisGraph->AxisNames.Underline);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_BOLD,ThisGraph->AxisLabels.Bold);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_COLOR,ThisGraph->AxisLabels.TextColor);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_ITALIC,ThisGraph->AxisLabels.Italic);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_POINT_SIZE,ThisGraph->AxisLabels.FontSize);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_STRIKEOUT,ThisGraph->AxisLabels.StrikeOut);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XYLABEL_UNDERLINE,ThisGraph->AxisLabels.Underline);

		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XAXIS_GAIN,ThisGraph->XAxis.AxisGain);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XAXIS_OFFSET,ThisGraph->XAxis.AxisOffset);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XDIVISIONS,ThisGraph->XAxis.Divisions);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XENG_UNITS,ThisGraph->XAxis.EngineeringUnits);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XFORMAT,ThisGraph->XAxis.Format);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XMAP_MODE,ThisGraph->XAxis.MapMode);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XMARK_ORIGIN,ThisGraph->XAxis.MarkOrigin);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XNAME,ThisGraph->XAxis.Name);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XPRECISION,ThisGraph->XAxis.Precision);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XREVERSE,ThisGraph->XAxis.Reverse);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XLABEL_VISIBLE,ThisGraph->XAxis.ShowLabel);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_XGRID_VISIBLE,ThisGraph->XAxis.ShowGrid);

		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YAXIS_GAIN,ThisGraph->YAxis.AxisGain);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YAXIS_OFFSET,ThisGraph->YAxis.AxisOffset);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YDIVISIONS,ThisGraph->YAxis.Divisions);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YENG_UNITS,ThisGraph->YAxis.EngineeringUnits);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YFORMAT,ThisGraph->YAxis.Format);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YMAP_MODE,ThisGraph->YAxis.MapMode);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YMARK_ORIGIN,ThisGraph->YAxis.MarkOrigin);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YNAME,ThisGraph->YAxis.Name);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YPRECISION,ThisGraph->YAxis.Precision);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YREVERSE,ThisGraph->YAxis.Reverse);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YLABEL_VISIBLE,ThisGraph->YAxis.ShowLabel);
		SetCtrlAttribute(ThisGraph->Panel,ThisGraph->Graph,ATTR_YGRID_VISIBLE,ThisGraph->YAxis.ShowGrid);

		SetAxisScalingMode(ThisGraph->Panel,ThisGraph->Graph,VAL_XAXIS,ThisGraph->XAxis.ScalingMode,
			ThisGraph->XAxis.Minimum,ThisGraph->XAxis.Maximum);
		SetAxisScalingMode(ThisGraph->Panel,ThisGraph->Graph,VAL_LEFT_YAXIS,ThisGraph->YAxis.ScalingMode,
			ThisGraph->YAxis.Minimum,ThisGraph->YAxis.Maximum);
		SetCtrlAttribute (ThisGraph->Panel,ThisGraph->Graph,ATTR_NUM_CURSORS,ListNumItems (ThisGraph->Cursors));

		for (i=1;i<=ListNumItems(ThisGraph->Cursors);i++)
		{
			ListGetItem (ThisGraph->Cursors, &ThisCursor,i);
			SetCursorAttribute (ThisGraph->Panel,ThisGraph->Graph,i,ATTR_CURSOR_COLOR,ThisCursor.Color);
			SetCursorAttribute (ThisGraph->Panel,ThisGraph->Graph,i,ATTR_CROSS_HAIR_STYLE,ThisCursor.CrosshairStyle);
			SetCursorAttribute (ThisGraph->Panel,ThisGraph->Graph,i,ATTR_CURSOR_MODE,ThisCursor.Mode);
			SetCursorAttribute (ThisGraph->Panel,ThisGraph->Graph,i,ATTR_CURSOR_POINT_STYLE,ThisCursor.PointStyle);
		}

    }
	return 0;
}
int CVICALLBACK CancelGraph(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    if(event==EVENT_COMMIT) RemovePopup (0);
	return 0;
}
//new callbacks

static int CVICALLBACK ChangeCursors(int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
    GraphType *ThisGraph;
	CursorType thiscursor;
	int cursornumber;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,CURSORS_CURSORNUMBER,&cursornumber);
		ListGetItem (ThisGraph->Cursors,&thiscursor,cursornumber);
		GetCtrlVal(panel,CURSORS_POINTSTYLE,&thiscursor.PointStyle);
		GetCtrlVal(panel,CURSORS_CROSSHAIRSTYLE,&thiscursor.CrosshairStyle);
		GetCtrlVal(panel,CURSORS_CURSORMODE,&thiscursor.Mode);
		GetCtrlVal(panel,CURSORS_CURSORCOLOR,&thiscursor.Color);
		ListReplaceItem (ThisGraph->Cursors,&thiscursor,cursornumber);
	}
	return 0;
}
static int CVICALLBACK ChangeNumberCursors (int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
	CursorType firstcursor;
	int NumberCursors,PrevNumberCursors;
	CursorType thiscursor;
	int i;
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		PrevNumberCursors=ListNumItems(ThisGraph->Cursors);
		GetCtrlVal(panel,CURSORS_NUMBERCURSORS,&NumberCursors);
		if((PrevNumberCursors==0)&&(NumberCursors>0))
		{
			SetCtrlAttribute(panel,CURSORS_CURSORCOLOR,ATTR_DIMMED,0);
			SetCtrlAttribute(panel,CURSORS_CURSORNUMBER,ATTR_DIMMED,0);
			SetCtrlAttribute(panel,CURSORS_CURSORMODE,ATTR_DIMMED,0);
			SetCtrlAttribute(panel,CURSORS_CROSSHAIRSTYLE,ATTR_DIMMED,0);
			SetCtrlAttribute(panel,CURSORS_POINTSTYLE,ATTR_DIMMED,0);
		}	
		if((PrevNumberCursors>0)&&(NumberCursors==0))
		{
			SetCtrlAttribute(panel,CURSORS_CURSORCOLOR,ATTR_DIMMED,1);
			SetCtrlAttribute(panel,CURSORS_CURSORNUMBER,ATTR_DIMMED,1);
			SetCtrlAttribute(panel,CURSORS_CURSORMODE,ATTR_DIMMED,1);
			SetCtrlAttribute(panel,CURSORS_CROSSHAIRSTYLE,ATTR_DIMMED,1);
			SetCtrlAttribute(panel,CURSORS_POINTSTYLE,ATTR_DIMMED,1);
		}	
		if(PrevNumberCursors>NumberCursors)
		{
			for(i=PrevNumberCursors;i>NumberCursors;i--) ListRemoveItem(ThisGraph->Cursors,0,END_OF_LIST);
		}	
		if(PrevNumberCursors<NumberCursors)
		{
			GetCtrlVal(panel,CURSORS_CURSORCOLOR,&thiscursor.Color);
			GetCtrlVal(panel,CURSORS_CURSORMODE,&thiscursor.Mode);
			GetCtrlVal(panel,CURSORS_CROSSHAIRSTYLE,&thiscursor.CrosshairStyle);
			GetCtrlVal(panel,CURSORS_POINTSTYLE,&thiscursor.PointStyle);
			for(i=PrevNumberCursors+1;i<=NumberCursors;i++) 
			{
				ListInsertItem(ThisGraph->Cursors,&thiscursor,END_OF_LIST);
			}
		}
		if(NumberCursors==0)
		{
			SetCtrlAttribute(panel,CURSORS_CURSORNUMBER,ATTR_MIN_VALUE,0);
			SetCtrlAttribute(panel,CURSORS_CURSORNUMBER,ATTR_MAX_VALUE,0);
			SetCtrlVal(panel,CURSORS_CURSORNUMBER,0);
		}
		else
		{
			SetCtrlAttribute(panel,CURSORS_CURSORNUMBER,ATTR_MAX_VALUE,NumberCursors);
			SetCtrlAttribute(panel,CURSORS_CURSORNUMBER,ATTR_MIN_VALUE,1);
			SetCtrlVal(panel,CURSORS_CURSORNUMBER,1);
		}
	}
	return 0;
}
static int CVICALLBACK ChangeCursorNumber (int panel,int control,int event,void *callbackData,int eventData1,int eventData2)
{
	int i,cursornumber;
	CursorType thiscursor;
    GraphType *ThisGraph;
    if(event==EVENT_COMMIT) 
    {
    	ThisGraph=(GraphType *)callbackData;
		GetCtrlVal(panel,CURSORS_CURSORNUMBER,&cursornumber);
		ListGetItem (ThisGraph->Cursors,&thiscursor,cursornumber);
		SetCtrlVal(panel,CURSORS_POINTSTYLE,thiscursor.PointStyle);
		SetCtrlVal(panel,CURSORS_CROSSHAIRSTYLE,thiscursor.CrosshairStyle);
		SetCtrlVal(panel,CURSORS_CURSORMODE,thiscursor.Mode);
		SetCtrlVal(panel,CURSORS_CURSORCOLOR,thiscursor.Color);
	}
	return 0;
}
