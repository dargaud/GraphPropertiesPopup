//Demonstrates the use of a Graph Properties Popup Panel
//See GraphPropertiesPopup.h for explanation

#include <cvirte.h>		/* Needed if linking in external compiler; harmless otherwise */
#include <userint.h>
#include "demo.h"
#include "GraphPropertiesPopup.h"

static int panelHandle;

int main (int argc, char *argv[]) {
	if (InitCVIRTE (0, argv, 0) == 0)	/* Needed if linking in external compiler; harmless otherwise */
		return -1;	/* out of memory */
	if ((panelHandle = LoadPanel (0, "demo.uir", PANEL)) < 0)
		return -1;
	DisplayPanel (panelHandle);
	RunUserInterface ();
	return 0;
}

int CVICALLBACK DemoPanel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	switch (event) {
		case EVENT_CLOSE:
			QuitUserInterface (0);
			break;
	}
	return 0;
}

int CVICALLBACK GraphProperties1 (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GraphPropertiesPopup(panel,PANEL_GRAPH1,0);
			break;
	}
	return 0;
}

int CVICALLBACK GraphProperties2 (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GraphPropertiesPopup(panel,PANEL_GRAPH2,3); //there are a minimimum of three cursors
			break;
	}
	return 0;
}
