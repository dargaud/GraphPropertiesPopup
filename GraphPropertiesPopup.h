//The main purpose of this routine is to allow the end user to completely customize the 
//graph that is being displayed.

//It takes the handle of the panel the graph is on, the graph control, and
//the minimum number of cursors as input.
//Almost every graph property is modifiable with the notable exception of 
//the right y axis (the code can be easily modified for this or other changes).

//The minimum number of cursors is set as a parameter so that, if cursors are used 
//in analysis, they are not removed by the end user. Set minimum cursors to zero 
//if you don't want to use this feature. If the graph already has less cursors 
//than the minimum number, then this has no effect.

//Four files are needed- GraphPropertiesPopup.c, GraphPropertiesPopup.h, 
//GraphPropertiesPopupUIR.uir, and GraphPropertiesPopupUIR.h
//GraphPropertiesPopup.h must be #included in a source file for that file
//to use this routine, but none of the others require an include statement.

//Also, the Programmer's Toolbox and Easy Tab Control must be loaded.

//To use a graph properties popup, this is the only call that is needed.
//Josh Reiss, August 17, 1999

#ifndef _GRAPH_PROPERTIES_POPUP
#define _GRAPH_PROPERTIES_POPUP

extern int GraphPropertiesPopup(int Panel,int Control,int MinimumCursors); 

#endif // _GRAPH_PROPERTIES_POPUP
